---
layout: post
title: First Post
date: 2015-09-30
---
You&#39;ll find this post in your `_posts` directory. Go ahead and edit it to see your changes.

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.md` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

You can also use CloudCannon&#39;s beautiful markdown editor to add and update posts.